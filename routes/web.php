<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});


Route::resource('barang', 'BarangController');
Route::get('/barang', 'BarangController@index')->name('indexBarang');

Route::resource('tt', 'TtController');
Route::get('/tt', 'TtController@index')->name('indexTt');

Route::resource('kembali', 'KembaliController');
Route::get('hitung', 'KembaliController@store');

Route::get('makepdfproduk','TtController@makepdf')->name('makePDF');

Route::get('printpdfproduk','TtController@printpdf')->name('cetakPDF');

Route::get('transaksi','TtController@showTransaksi')->name('showTr');

Route::get('printtransaksi','TtController@printTransaksi')->name('cetakTR');

Auth::routes();


Route::get('add', function () {
    return view('add');
});

Route::get('/home', function(){
	return redirect('/tt');
});
