@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="row">
		     <div class="panel-heading col-10">
		        <h3>Converse Item</h3>					
		     </div>
				<div class="text-right mb-3">		
		    <a href="{{ route('barang.create')}}" class="btn btn-success pull-right" style="margin-top:-8px"> Tambah Data</a>
		   </div>
    	</div>	
	   <div class="panel-body ">{{csrf_field()}}       
      <table class="table table-striped">
        <thead class="thead-dark">
         <tr><th scope="col">ID Barang</th>
              <th scope="col">Nama Produk</th>
              <th scope="col">Stok</th>
              <th scope="col">Harga Awal</th> 
              <th scope="col">Discount</th>
              <th scope="col">Harga Akhir</th>
              <th scope="col"></th> </tr>
          </thead>
          <tbody>
          @foreach($barang as $data)
          <tr><th scope="row">{{$data -> barang_id}}</th>
              <td>{{$data -> nama}}</td>
              <td>{{$data -> stok}}</td>
              <td>@currency($data -> harga_awal)</td>
              <td>{{$data -> discount}} %</td>
              <td>@currency($data -> harga_akhir)</td>
              <td>                           
				          <form method="POST" action="{{ route('barang.destroy', $data->barang_id) }}">
					         {{ csrf_field() }} {{ method_field('DELETE')}}
					         <a href="{{ route('barang.edit', $data->barang_id) }}" class="btn btn-warning"> Edit </a>
					         <button type="submit" class="btn btn-secondary"> Hapus </button>
				          </form>              
              </td></tr>
          @endforeach
          </tbody>
        </table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection