@extends('layouts.app1')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4> Tambah Produk </h4>
			  </div>
			<div class="container mt-5"> 	
				 <form action="{{ route('barang.store')}}" method="POST">                        
            {{ csrf_field() }}
         @if($errors->any())
          <div class="alert alert-danger"><ul>@foreach($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach </ul></div>
         @endif

                <div class="col-md-6"><div class="form-group">
                      <label for="exampleInputEmail1">ID</label>
                      <input type="text" readonly name="idB" class="form-control" value = "{{ $nobar->id }}" id="exampleInputEmail1" placeholder="Nama" required="">
                </div></div>

                <div class="col-md-6"><div class="form-group">
                      <label for="exampleInputEmail1">Nama</label>
                      <input type="text" name="nama" class="form-control" value = "" id="exampleInputEmail1" placeholder="Nama" required="">
                </div></div>

                <div class="col-md-6"><div class="form-group">
                      <label for="exampleInputPassword1">Harga Awal(Rp)</label>
                      <input type="number" name="harga_awal" class="form-control" value = "" id="exampleInputPassword1" placeholder="harga awal" required="">
                </div></div>

                <div class="col-md-6"><div class="form-group">
                      <label for="exampleInputEmail1">Discount(%)</label>
                      <input type="number" name="discount" class="form-control" id="exampleInputEmail1" value = "" placeholder="Discount" required="">
                </div></div>

                <div class="col-md-6"><div class="form-group">
                      <label for="exampleInputEmail1">Stock</label>
                      <input type="text" name="stok" class="form-control" value = "" id="exampleInputEmail1" placeholder="stok" required="">
                </div></div>

                <div class="col-md-6">
                      <button type="submit" class="btn btn-primary btn-block btn-primary btn-submit">Submit</button>
                </div>
          </form>
			</div>
	</div>
</div>
</div>
</div>
@endsection