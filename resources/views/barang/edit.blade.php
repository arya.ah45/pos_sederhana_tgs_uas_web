@extends('layouts.app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4> Edit Produk </h4>
      	</div>
			<div class="panel-body">
			<form class="form-horizontal" role="form" method="POST" action="{{ route('barang.update',$barang->barang_id)}}">
			{{ csrf_field() }} {{ method_field('PATCH')}}
     @if($errors->any())
          <div class="alert alert-danger">
            <ul>@foreach($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach</ul>
          </div>@endif

     			<div class="col-md-6"><div class="form-group">
                      <label for="exampleInputEmail1">Nama</label>
                      <input type="text" name="nama" class="form-control" value = "{{ $barang->nama }}" id="exampleInputEmail1" placeholder="Nama" >
          </div></div>
          <div class="col-md-6"><div class="form-group">
                      <label for="exampleInputPassword1">Harga Awal(Rp)</label>
                      <input type="number" name="harga_awal" class="form-control" value = "{{ $barang->harga_awal }}" id="exampleInputPassword1" placeholder="harga awal" >
          </div></div>
          <div class="col-md-6"><div class="form-group">
                      <label for="exampleInputEmail1">Discount(%)</label>
                      <input type="number" name="discount" class="form-control" id="exampleInputEmail1" value = "{{ $barang->discount }}" placeholder="Discount" >
          </div></div>
          <div class="col-md-6"><div class="form-group">
                      <label for="exampleInputEmail1">Stock</label>
                      <input type="text" name="stok" class="form-control" value = "{{ $barang->stok }}" id="exampleInputEmail1" placeholder="Nama" >
          </div></div>
          <div class="col-md-6">
              <button type="submit" class="btn btn-primary btn-block btn-primary btn-submit">Submit</button>
          </div>
      </form>
			</div>
	</div>
</div></div></div>
@endsection