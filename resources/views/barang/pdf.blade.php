<!DOCTYPE html>
<html>
 <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Produk PDF</title>
  </head>
  <body>
   <hr><h2><img src="{{('img/Logo-Converse.png')}}" height="50"> CONVERSE INVOICE </h2><hr>
      <br>
<div class="container-fluid">
<div class="row">
<div class="col-5">
  <table class="table"> 
    <tbody>
    <tr>
      <td scope="row">Invoice Number</td>
      <td> : </td>
      <td>{{$invoice -> invoice}}</td>
    </tr>
    <tr>
      <td scope="row">Invoice Date</td>
      <td> : </td>
      <td>{{$inv -> waktu_transaksi}}</td>
    </tr>
    </tbody>
  </table>
</div>
</div>

      <h4>Item</h4>
       {{csrf_field()}}       
        <table class="table table-striped">
          <thead class="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Nama Produk</th>
              <th scope="col">Harga_Awal</th>
              <th scope="col">Diskon</th>
              <th scope="col">Harga_Akhir</th>
              <th scope="col">Qty</th>
              <th scope="col">Total</th>
            </tr>
          </thead>
         <tbody>
          @foreach($brg as $temp)
            <tr>
              <th scope="row">{{++$no}}</th>
              <th>{{$temp -> nama_barang}}</th>
              <td>@currency($temp -> harga_awal)</td>
              <td>{{$temp -> discount}} %</td>
              <td>@currency($temp -> harga_akhir)</td>
              <td>{{$temp -> qty}}</td>
              <td>@currency($temp -> total)</td>
            </tr>
          @endforeach
          </tbody>
        </table>
        <table class="table">
          <thead>
            <tr>
              <th scope="col">Detil Pembayaran</th>
              <th scope="col"></th>
              <th scope="col"></th>                            
            </tr>
          </thead>
          <tbody>
            <tr>
              <td scope="row">Total</td>
              <td>:</td>
              <td>@currency($inv->total_harga)</td>                            
            </tr>
            <tr>
              <td scope="row">Pembayaran</td>
              <td>:</td>
              <td>@currency($inv->bayar)</td>                            
            </tr>
            <tr>
              <td scope="row">Kembalian</td>
              <td>:</td>
              <td>@currency($inv->kembalian)</td>                            
            </tr>                        
          </tbody>
        </table> 
        <hr>

</div>
</body>
</html>