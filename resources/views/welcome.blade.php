@extends('layouts.app1')

@section('content')
<html lang="{{ app()->getLocale() }}">
    <head>
        <title>Arka POS</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <style >
        .my-custom-scrollbar {
        position: relative;
        height: 420px;
        overflow: auto;
        }
        .table-wrapper-scroll-y {
        display: block;
        }
    </style>
    </head>

    <body>
    <div class="container">
      <div class="row">

      <div class="col-6">
      <h3>Converse Item</h3>
      <div class="card text-white bg-dark mb-3" style="max-width: 36rem;">
      
       {{csrf_field()}}       
        <table class="table table-striped table-dark table-wrapper-scroll-y my-custom-scrollbar">
          <thead>
            <tr>
              <th scope="col">ID Barang</th>
              <th scope="col">Nama Produk</th>
              <th scope="col">Stok</th>
              <th scope="col">Discount</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
          @foreach($barang as $data)
            <tr>
              <th scope="row">{{$data -> barang_id}}</th>
              <td>{{$data -> nama}}</td>
              <td>{{$data -> stok}}</td>
              <td>{{$data -> discount}}%</td>
              <td>                           
              <a href= "{{ route('tt.edit', $data->barang_id) }}" class="btn btn-primary">+</a>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
      </div>
      </div>

      <div class="col-6">
      <h3>Converse Payment</h3>
      
      <div class="card text-right text-white bg-dark mb-3" style="max-width: 36rem;">
      <form method="POST" action="{{route('kembali.store')}}">
      <div class="card-body">
      <h5 class="card-title">Bayar</h5>
      <div class="form-group"> 

        {{ csrf_field() }} 
           @if($errors->any())
          <div class="alert alert-danger"><ul>
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul>
          </div>
          @endif   

              <input hidden type="text" name="totalnya" class="form-control" id="exampleInputPassword1" placeholder="Rp.0,-" value="{{$totalnya}}">
             
              <input type="text" name="uang" class="form-control" value ="" id="exampleInputPassword1" placeholder="Rp. ">
        
        </div>
                 <button type="submit" class="btn btn-primary">Hitung</button>
        </div>
        </form>
        <div class="card-header">
        <table class="table table-dark">
          <thead>
            <tr>
              <th scope="col">Detil Pembayaran</th>
              <th scope="col"></th>
              <th scope="col"></th>                            
            </tr>
          </thead>
          <tbody>
            <tr>
              <td scope="row">Total</td>
              <td>:</td>
              <td>@currency($totalnya)</td>                            
            </tr>
            <tr>
              <td scope="row">Pembayaran</td>
              <td>:</td>
              <td>@currency($kembalian->uang)</td>                            
            </tr>
            <tr>
              <td scope="row">Kembalian</td>
              <td>:</td>
              <td>@currency($kembalian->kembalian)</td>                            
            </tr>                        
          </tbody>
        </table> 
        </div>
        <a href="{{ route('cetakPDF')}}" class="btn btn-success pull-right" style="margin-top:-8px"> Simpan & Cetak</a>
      </div>  
      </div>

      <div class="container ">
     
      <h3>Cart</h3>
       {{csrf_field()}}       
        <table class="table table-striped">
          <thead class="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Nama Produk</th>
              <th scope="col">Harga_Awal</th>
              <th scope="col">Diskon</th>
              <th scope="col">Harga_Akhir</th>
              <th scope="col">Qty</th>
              <th scope="col">Total</th>
              <th scope="col"></th>
            </tr>
          </thead>
          <tbody>
          @foreach($tempT as $temp)
            <tr>
              <th scope="row">{{++$no}}</th>
              <th>{{$temp -> nama}}</th>
              <td>@currency($temp -> harga_awal)</td>
              <td>{{$temp -> discount}} %</td>
              <td>@currency($temp -> harga_akhir)</td>
              <td>{{$temp -> qty}}</td>
              <td>@currency($temp -> total)</td>
              <td>                           
              <form method="POST" action="{{ route('tt.destroy', $temp->tt_id) }}">
                {{ csrf_field() }} {{ method_field('DELETE')}}
                <button type="submit" class="btn btn-danger">X</button>
              </form>
              </td>
            </tr>
          @endforeach
          </tbody>
        </table>
      </div>
      </div>

    
    </body>
</html>

@endsection