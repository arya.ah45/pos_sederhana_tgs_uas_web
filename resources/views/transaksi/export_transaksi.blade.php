<html lang="{{ app()->getLocale() }}">
    <head>
        <title>Arka POS</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    </head>

    <body>
    <div class="container-fluid">
<hr>
    <h3><center> ~ Converse Transaction Report ~ </center></h3>
<hr>
<br>
       {{csrf_field()}}       
        <table class="table table-striped ">
          <thead class="thead-dark">
            <tr>
              <th scope="col">ID Transaksi</th>
              <th scope="col">Waktu Transaksi</th>
              <th scope="col">Produk Terjual</th>
              <th scope="col">Harga Awal</th>
              <th scope="col">Discount</th>
              <th scope="col">Harga Akhir</th>
              <th scope="col">Qty</th>
              <th scope="col">Total</th>
            </tr>
          </thead>
          <tbody>
          @foreach($transaksi as $data)
            <tr>
              <th scope="row">{{$data -> id_transaksi}}</th>
              <td>{{$data -> waktu_transaksi}}</td>
              <td>{{$data -> nama_barang}}</td>
              <td>@currency($data -> harga_awal)</td>
              <td>{{$data -> discount}} %</td>
              <td>@currency($data -> harga_akhir)</td>
              <td>{{$data -> qty}}</td>
              <td>@currency($data -> total)</td>                                                        
            </tr>
          @endforeach
          </tbody>
        </table>
        <div class="col-5">
        <table class="table" style="border: none;"> 
          @foreach($rekapTR as $tr)
          <tbody>
          <tr>
            <td scope="row"><strong>Omset</strong></td>
            <td> : </td>
            <td>@currency($tr -> omset)</td>
          </tr>
          <tr>
            <td scope="row"><strong>Produk Terjual</strong></td>
            <td> : </td>
            <td>{{$tr -> pt}}</td>
          </tr>
          </tbody>
          @endforeach
        </table>
      </div>
      </div>

    
    </body>
</html>
