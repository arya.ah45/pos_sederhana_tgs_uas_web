<html>
    <head>
        <title>Arka POS</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    </head>

    <body>
    <div class="container mt-5">
      <div class="col">
        <center><h4><b>Add Sub Item</b></h4></center>                 
      <form action="{{ route('tt.store')}}" method="POST">                        
       
       {{ csrf_field() }}
       @if($errors->any())
          <div class="alert alert-danger"><ul>
            @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
          </ul></div>
          @endif

          <div class="row">
              <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nama</label>
                    <input type="text" name="nama" class="form-control" value = "{{ $barang->nama }}" id="exampleInputEmail1" placeholder="Nama" readonly="">
                  </div>
              </div>

              <div class="col-md-6">
                  <div class="form-group">
                    <label for="exampleInputPassword1">Harga Awal(Rp)</label>
                    <input type="number" name="harga_awal" class="form-control" value = "{{ $barang->harga_awal }}" id="exampleInputPassword1" placeholder="harga awal" readonly="">
                  </div>
              </div>
          </div>

          <div class="row">

          <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputEmail1">Discount(%)</label>
                <input type="number" name="discount" class="form-control" id="exampleInputEmail1" value = "{{ $barang->discount}}" placeholder="Discount" readonly="">
              </div>
          </div>

          <div class="col-md-6">
              <div class="form-group">
                <label for="exampleInputPassword1">Harga Akhir(Rp)</label>
                <input type="number" name="harga_akhir" class="form-control" value = "{{ $barang->harga_akhir}}" id="exampleInputPassword1" placeholder="harga akhir" readonly="">
              </div>
          </div>
          </div>

          <input type="hidden" name="barang_id" value="{{ $barang->barang_id}}">

          <div class="row">

               <div class="col-md-5 col-md-offset-4">
                  <div class="form-group">
                    <center><label for="exampleInputEmail1">- Jumlah barang -</label></center>
                    <input type="number" name="qty" value="" class="form-control" id="exampleInputEmail1">
                  </div>
              </div>
          </div>
                        
          <button type="submit" class="btn btn-primary btn-block btn-primary btn-submit">
          <span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Submit</button>
        </form>
    </div>
    </div>
    </body>