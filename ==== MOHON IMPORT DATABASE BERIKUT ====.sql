/*
SQLyog Ultimate v12.5.1 (64 bit)
MySQL - 10.1.37-MariaDB : Database - pos_new
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`pos_new` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `pos_new`;

/*Table structure for table `barang` */

DROP TABLE IF EXISTS `barang`;

CREATE TABLE `barang` (
  `barang_id` varchar(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `stok` int(11) DEFAULT NULL,
  `harga_awal` int(11) NOT NULL,
  `discount` int(11) NOT NULL DEFAULT '0',
  `harga_akhir` int(11) NOT NULL,
  PRIMARY KEY (`barang_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `barang` */

insert  into `barang`(`barang_id`,`nama`,`stok`,`harga_awal`,`discount`,`harga_akhir`) values 
('br-0001','Converse Jack Purcell',0,9009000,99,90090),
('br-0002','Converse Courlandt',2,659000,25,494250),
('br-0003','Converse Star Replay',1,659000,20,527200),
('br-0004','Converse El Distro',2,559000,60,223600),
('br-0005','Converse Classic Chuck',0,769999,90,77000),
('br-0006','Converse Chuck Taylor All Star',1,599000,0,12000),
('br-0007','Converse Chuck 70',7,659000,5,626050),
('br-0008','Converse All Star New',8,500000,50,250000);

/*Table structure for table `detil_transaksi` */

DROP TABLE IF EXISTS `detil_transaksi`;

CREATE TABLE `detil_transaksi` (
  `id_transaksi` varchar(20) DEFAULT NULL,
  `nama_barang` varchar(30) DEFAULT NULL,
  `harga_awal` int(11) DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `harga_akhir` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `waktu_transaksi` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `detil_transaksi` */

insert  into `detil_transaksi`(`id_transaksi`,`nama_barang`,`harga_awal`,`discount`,`harga_akhir`,`qty`,`total`,`waktu_transaksi`) values 
('TR-20191226104536','Converse Star Replay',659000,20,527200,1,527200,'2019-12-26 10:47:05'),
('TR-20191226104536','Converse El Distro',559000,60,223600,1,223600,'2019-12-26 10:47:05'),
('TR-20191226104720','Converse El Distro',559000,60,223600,1,223600,'2019-12-26 10:47:57');

/*Table structure for table `id_transaksi` */

DROP TABLE IF EXISTS `id_transaksi`;

CREATE TABLE `id_transaksi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `IDTR` varchar(20) DEFAULT NULL,
  `invoice` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `id_transaksi` */

insert  into `id_transaksi`(`id`,`IDTR`,`invoice`) values 
(1,'TR-20191226104812','INV-20191226104812');

/*Table structure for table `idbarang` */

DROP TABLE IF EXISTS `idbarang`;

CREATE TABLE `idbarang` (
  `id` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `idbarang` */

insert  into `idbarang`(`id`) values 
('br-0009');

/*Table structure for table `kembalian` */

DROP TABLE IF EXISTS `kembalian`;

CREATE TABLE `kembalian` (
  `id` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `totalnya` int(11) DEFAULT NULL,
  `uang` int(11) DEFAULT NULL,
  `kembalian` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `kembalian` */

insert  into `kembalian`(`id`,`totalnya`,`uang`,`kembalian`,`created_at`,`updated_at`) values 
('0',0,0,0,'2019-12-26 03:48:34','2019-12-26 03:48:34');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2019_12_18_143533_create_kembalian',1);

/*Table structure for table `tb_transaksi` */

DROP TABLE IF EXISTS `tb_transaksi`;

CREATE TABLE `tb_transaksi` (
  `id_transaksi` varchar(20) DEFAULT NULL,
  `total_harga` int(11) DEFAULT NULL,
  `bayar` int(11) DEFAULT NULL,
  `kembalian` int(11) DEFAULT NULL,
  `waktu_transaksi` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tb_transaksi` */

insert  into `tb_transaksi`(`id_transaksi`,`total_harga`,`bayar`,`kembalian`,`waktu_transaksi`) values 
('TR-20191226104536',750800,760000,9200,'2019-12-26 10:47:05'),
('TR-20191226104720',223600,224000,400,'2019-12-26 10:47:57');

/*Table structure for table `temp_transaksi` */

DROP TABLE IF EXISTS `temp_transaksi`;

CREATE TABLE `temp_transaksi` (
  `tt_id` varchar(20) DEFAULT NULL,
  `nama` varchar(45) DEFAULT NULL,
  `harga_awal` int(11) DEFAULT NULL,
  `discount` int(11) DEFAULT NULL,
  `harga_akhir` int(11) DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  `barang_id` varchar(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `temp_transaksi` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`password`,`remember_token`,`created_at`,`updated_at`) values 
(1,'arya','arya@gmail.com','$2y$10$4QVtq9Hk6rYjlFpdYvAeFO/Yu993Bm0Sss8ZYsDNkdzNOxMcci2rK','lN3gaDxtjWDpsxRScgEP3B5rB5LMVZou0zkWPq1E8khalGKIMBuekWtfNNqe','2019-12-18 16:22:58','2019-12-18 16:22:58'),
(2,'sinku','a@example.com','$2y$10$ZHzKm3ttNVt9uEFs6THYcO1GPLxxpZKessacQ/3vMLOlYiSLMZhQK','3Rny3gwOneZ7E4mV2YqH5zb2VF92SadCz88X7idyZc0r6wWwZTEfuecoEDzh','2019-12-20 02:48:36','2019-12-20 02:48:36');

/* Trigger structure for table `temp_transaksi` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `jual_barang` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `jual_barang` AFTER INSERT ON `temp_transaksi` FOR EACH ROW BEGIN  
    UPDATE barang
    SET stok = stok - new.qty
    WHERE
    barang_id = new.barang_id;
    END */$$


DELIMITER ;

/* Trigger structure for table `temp_transaksi` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `jual_barang_UP` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `jual_barang_UP` AFTER UPDATE ON `temp_transaksi` FOR EACH ROW BEGIN  
    UPDATE barang
    SET stok = stok - new.qty
    WHERE
    barang_id = new.barang_id;
    END */$$


DELIMITER ;

/* Trigger structure for table `temp_transaksi` */

DELIMITER $$

/*!50003 DROP TRIGGER*//*!50032 IF EXISTS */ /*!50003 `g_jual_barang` */$$

/*!50003 CREATE */ /*!50017 DEFINER = 'root'@'localhost' */ /*!50003 TRIGGER `g_jual_barang` AFTER DELETE ON `temp_transaksi` FOR EACH ROW BEGIN
	UPDATE barang
	SET stok = stok + old.qty
	WHERE
	barang_id = old.barang_id;
	END */$$


DELIMITER ;

/* Function  structure for function  `newID` */

/*!50003 DROP FUNCTION IF EXISTS `newID` */;
DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` FUNCTION `newID`() RETURNS char(20) CHARSET latin1
    READS SQL DATA
BEGIN
DECLARE hasil CHAR(20);
SELECT id INTO hasil FROM (SELECT CONCAT('br-',(SELECT LPAD((SELECT IFNULL((SELECT SUBSTR(barang_id,5) FROM barang 
ORDER BY LPAD(SUBSTR(barang_id,5), 4, '0') DESC LIMIT 1), 0) + 1 AS id),4,'0'))) AS 'id')c;
RETURN(hasil);
END */$$
DELIMITER ;

/* Procedure structure for procedure `get_IDBRG` */

/*!50003 DROP PROCEDURE IF EXISTS  `get_IDBRG` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `get_IDBRG`()
BEGIN
truncate idbarang;
insert into idbarang value((SELECT CONCAT('br-',(SELECT LPAD((SELECT IFNULL((SELECT SUBSTR(barang_id,5) FROM barang 
ORDER BY LPAD(SUBSTR(barang_id,5), 4, '0') DESC LIMIT 1), 0) + 1 AS id),4,'0'))) AS 'id')
);
END */$$
DELIMITER ;

/* Procedure structure for procedure `newID_brg` */

/*!50003 DROP PROCEDURE IF EXISTS  `newID_brg` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `newID_brg`()
BEGIN

SELECT CONCAT('br-',(SELECT LPAD((SELECT IFNULL((SELECT SUBSTR(barang_id,5) FROM barang 
ORDER BY LPAD(SUBSTR(barang_id,5), 4, '0') DESC LIMIT 1), 0) + 1 AS id),4,'0'))) AS 'id';

END */$$
DELIMITER ;

/* Procedure structure for procedure `reset_transaksi` */

/*!50003 DROP PROCEDURE IF EXISTS  `reset_transaksi` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `reset_transaksi`()
BEGIN

TRUNCATE kembalian;
TRUNCATE temp_transaksi;
TRUNCATE id_transaksi;

insert into id_transaksi value(0,(SELECT CONCAT('TR-',(SELECT REPLACE(REPLACE(REPLACE( NOW(), '-', ''),' ',''),':',''))) AS 'IDTR'),
(SELECT CONCAT('INV-',(SELECT REPLACE(REPLACE(REPLACE( NOW(), '-', ''),' ',''),':',''))) AS 'inv')); 

END */$$
DELIMITER ;

/* Procedure structure for procedure `save_transaksi` */

/*!50003 DROP PROCEDURE IF EXISTS  `save_transaksi` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `save_transaksi`()
BEGIN

insert into detil_transaksi(id_transaksi,nama_barang,harga_awal,discount,harga_akhir,qty,total)
SELECT tt_id,nama,harga_awal,discount,harga_akhir,qty,total FROM temp_transaksi;

insert into tb_transaksi(id_transaksi,total_harga,bayar,kembalian) select id,totalnya,uang,kembalian from kembalian;

END */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
