<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TR extends Model
{
    protected $table = 'temp_transaksi';
    protected $primayKey = 'tt_id';
    protected $fillable = ['tt_id','nama','harga_awal','discount','harga_akhir','qty','barang_id','total'];
}
