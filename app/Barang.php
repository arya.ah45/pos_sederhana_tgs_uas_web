<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table = 'barang';
    protected $primayKey = 'barang_id';
    protected $fillable = ['barang_id','nama','stok','harga_awal','discount','harga_akhir'];
}
