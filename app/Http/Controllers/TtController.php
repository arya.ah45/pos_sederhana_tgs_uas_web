<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TR;
use App\Barang;
use App\Kembalian;
use PDF;
use Redirect;

class TtController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    { 
       $no=0;
       $barang = \DB::select('select * from barang');
       $tempT = TR::all();
       $totalnya = \DB::table('temp_transaksi')->sum('total');
       $cek_kembalian = \DB::table('kembalian')->count();

       if($cek_kembalian > 0){
       $kembalian = \DB::table('kembalian')->orderBy('created_at', 'desc')->first();
       }else{
        $kembali = new Kembalian;
        $kembali->id = '0';
        $kembali->totalnya = 0;
        $kembali->uang = 0;
        $kembali->kembalian = 0;
        $kembali->save();
        $kembalian = \DB::table('kembalian')->orderBy('created_at', 'desc')->first();        
       }


       return view('welcome', compact('barang','tempT','no','totalnya','kembalian'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $barang_id = $request->barang_id;
        $qtyNew = $request->qty;
        $harga_akhir = $request['harga_akhir'];

    $cek = \DB::table('temp_transaksi')->where('barang_id',$barang_id)->count();
        //untuk mengecek apakah sudah ada data dengan id yang sama
        if($cek > 0){ //jika ada maka akan mengekeksekusi baris dibawah ini
            $qtyNow = \DB::table('temp_transaksi')->where('barang_id',$barang_id)->value('qty');
            $totalNow = \DB::table('temp_transaksi')->where('barang_id',$barang_id)
            ->value('total');
            $totalNew = $harga_akhir * $qtyNew;
            $totalUpdate = $totalNew + $totalNow;
            \DB::table('temp_transaksi')->where('barang_id',$barang_id)->update([
                'qty'=>$qtyNow+$qtyNew,'total'=>$totalUpdate]);
            
        }else{ //jika tidak maka akan menambah baris baru 

        $IDTR = \DB::table('id_transaksi')->value('IDTR');    
        $produk = new TR;
        $produk->tt_id = $IDTR;
        $produk->nama = $request['nama'];
        $produk->harga_awal = $request['harga_awal'];
        $produk->discount = $request['discount'];
        $produk->harga_akhir = $harga_akhir;
        $produk->qty = $request['qty'];
        $produk->barang_id = $barang_id;        
        $produk->total = $request['harga_akhir']*$request['qty'];
        $produk->save();
        }
          return Redirect('/tt');  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $barang = \DB::table('barang')->where('barang_id',$id)->first();
        
        
        return view('add', compact('barang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    public function makepdf()
    {
       \DB::select("CALL save_transaksi()");
       $tempT = TR::all();
       $kembalian = \DB::table('kembalian')->orderBy('created_at', 'desc')->first();
       $inv = \DB::table('tb_transaksi')->orderBy('waktu_transaksi', 'desc')->first();
       $invoice = \DB::table('id_transaksi')->first();
       $no = 0;


         \DB::select(\DB::RAW("CALL reset_transaksi()"));        
        return view('barang.pdf',compact('tempT','no','inv','invoice'));


    }

    public function printpdf(){
       \DB::select("CALL save_transaksi()");
       $tempT = TR::all();
       $kembalian = \DB::table('kembalian')->orderBy('created_at', 'desc')->first();
       $inv = \DB::table('tb_transaksi')->orderBy('waktu_transaksi', 'desc')->first();
       $brg = \DB::select("SELECT * FROM detil_transaksi WHERE id_transaksi = 
(SELECT id_transaksi FROM detil_transaksi ORDER BY waktu_transaksi DESC LIMIT 1)");
       $invoice = \DB::table('id_transaksi')->first();
       $no = 0;
 
       $pdf = PDF::loadView('barang.pdf',compact('no','inv','invoice','brg'));
       $pdf->setPaper('a4', 'landscape');
       
       \DB::select(\DB::RAW("CALL reset_transaksi()")); 
       return $pdf->download();
       return Redirect('/tt'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::table('temp_transaksi')->where('tt_id',$id)->delete();

          return Redirect('/tt'); 
    }

    public function showTransaksi()
    {
       $transaksi = \DB::select('select * from detil_transaksi');
       $rekapTR = \DB::select( \DB::RAW("SELECT SUM(total) AS 'omset',SUM(qty) AS 'pt'  FROM detil_transaksi"));

       return view('transaksi.index_transaksi', compact('transaksi','rekapTR'));        
    }

    public function printTransaksi()
    {
       $transaksi = \DB::select('select * from detil_transaksi');
       $rekapTR = \DB::select( \DB::RAW("SELECT SUM(total) AS 'omset',SUM(qty) AS 'pt'  FROM detil_transaksi"));
       // return view('transaksi.export_transaksi', compact('transaksi','rekapTR'));    
       $pdf = PDF::loadView('transaksi.export_transaksi', compact('transaksi','rekapTR'));
       $pdf->setPaper('a4', 'portrait');
       return $pdf->download();
       
    }
}
