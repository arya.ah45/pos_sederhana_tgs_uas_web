<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Barang;
use App\TR;
use Redirect;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  $no=0;
       $barang = Barang::all();

       return view('barang.index', compact('barang','no'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        \DB::select("CALL get_IDBRG()");
        $nobar = \DB::table('idbarang')->first();
        return view('barang.create', compact('nobar'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $id_brg = $request->idB;
        $nama = $request->nama;
        $stok = $request->stok;
        $harga_awal = $request->harga_awal;
        $discount = $request->discount;
        $harga = 100 - $discount;
        $harga_akhir = $harga_awal * $harga / 100;


        \DB::table('barang')->insert([
            'barang_id'=>$id_brg,
            'nama'=>$nama,
            'stok'=>$stok,
            'harga_awal'=>$harga_awal,
            'discount'=>$discount,
            'harga_akhir'=>$harga_akhir ]
        );

        return Redirect('/barang');  

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function edit($id)
    {
        $barang = \DB::table('barang')->where('barang_id',$id)->first();
        
        
        return view('barang.edit', compact('barang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $nama = $request->nama;
        $stok = $request->stok;
        $harga_awal = $request->harga_awal;
        $discount = $request->discount;
        $harga = 100 - $discount;
        $harga_akhir = $harga_awal * $harga / 100;

       \DB::table('barang')
            ->where('barang_id',$id)
            ->update([            
            'nama'=>$nama,
            'stok'=>$stok,
            'harga_awal'=>$harga_awal,
            'discount'=>$discount,
            'harga_akhir'=>$harga_akhir 
            ]);
        return Redirect('/barang'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::table('barang')->where('barang_id',$id)->delete();

          return Redirect('/barang'); 
    }
}
