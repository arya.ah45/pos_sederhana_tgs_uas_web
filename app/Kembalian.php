<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kembalian extends Model
{
    protected $table = 'kembalian';
    protected $primayKey = 'id';
    protected $fillable = ['totalnya','uang','kembalian'];
}
